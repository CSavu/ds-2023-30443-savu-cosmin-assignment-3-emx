import React, {useEffect, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Footer from '../containers/Footer';
import { authenticateUser } from '../services/authentication-service';
import Cookies from 'js-cookie';
import { useAuth } from '../context/AuthContext';

function Login(props) {
    const { isUserLoggedIn, refreshAuthorization, isUserAdmin, isAuthInProgress } = useAuth();
    const [{user, pass}, setReg] = useState({user: '', pass: ''});
    const [resp, setResp] = useState('');
    const [isWrongCredentials, setWrongCredentials] = useState(false);

    function handleChange(event) {
        const inputName = event.target.name;
        const newVal = event.target.value;

        setReg(prevVal => {
            if (inputName === 'username') {
                return {user: newVal, pass: prevVal.pass};
            } else if (inputName === 'password') {
                return {user: prevVal.user, pass: newVal};
            }
        });
    }

    async function handleSubmit(event) {
        event.preventDefault(); // Prevent the default form submission
    
        const authResponse = await authenticateUser(user, pass);
        console.log(authResponse);
        setResp(authResponse);
    }

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        if (resp?.status === 'FAIL' || resp?.error === 'Unauthorized') {
            setWrongCredentials(true);
        } else if (resp?.userId) {
            //props.success('success');
            console.log(isUserLoggedIn);
            console.log(resp.token);
            Cookies.set('token', resp.token);
            refreshAuthorization();
            if (resp?.role === 'ADMIN') {
                window.location.href = '/users';
            } else if (resp?.role === 'REGULAR_USER') {
                window.location.href = '/profile';
            }
            
            //document.cookie = 'userId=' + resp.response + ';';
            //window.location.href = '/profile';
        }
    }, [props, resp]);

    if (!isAuthInProgress && !isUserLoggedIn) {
        return (
            <div>
                <div id="login">
                    <div className="container mt-5">
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="card mx-auto text-left">
                                    <div className="card-body">
                                        <form onSubmit={handleSubmit}>
                                            <div className="form-group">
                                                <TextField required placeholder="Username" type="text"
                                                    className="form-control" name="username"
                                                    onChange={handleChange} value={user}/>
                                            </div>
                                            <div className="form-group">
                                                <TextField required placeholder="Password" type="password"
                                                    className="form-control" name="password"
                                                    onChange={handleChange} value={pass}/>
                                            </div>
                                            {
                                                isWrongCredentials && <div className="form-group">
                                                    <h5 style={{color: 'red'}}>
                                                        Incorrect Username or Password
                                                    </h5>
                                                </div>
                                            }
                                            <button type="submit" className="btn">Login</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    } else if (!isAuthInProgress && isUserAdmin) {
        window.location.href = '/users';
        return null;
    } else if (!isAuthInProgress && !isUserAdmin) {
        window.location.href = '/profile';
        return null;
    }
}

export default Login;