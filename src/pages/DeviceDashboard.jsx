import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { useAuth } from '../context/AuthContext';
import { getAllDevices, deleteDevice, createDevice, updateDevice } from '../services/device-management-service';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import { addDeviceToUser } from '../services/device-management-service';

function ListItemLink(props) {
    return <ListItem button component={Link} {...props} />;
}

function DeviceDashboard() {
    const { isUserLoggedIn, refreshAuthorization, isUserAdmin, isAuthInProgress } = useAuth();
    //const { user } = {};
    const [devices, setDevices] = useState([]);
    const [deviceName, setDeviceName] = useState('');
    const [deviceAddress, setDeviceAddress] = useState('');
    const [deviceMaximumHourlyEnergyConsumption, setDeviceMaximumHourlyEnergyConsumption] = useState('');
    const [mappingDeviceId, setMappingDeviceId] = useState('');
    const [mappingUserId, setMappingUserId] = useState('');
    const [editedDeviceInfo, setEditedDeviceInfo] = useState({});

    const handleEditClick = (deviceId, name, address) => {
        setEditedDeviceInfo({
            ...editedDeviceInfo,
            [deviceId]: { name, address },
        });
    };

    const handleSaveClick = async (deviceId) => {
        const { name, address } = editedDeviceInfo[deviceId];

        const updateResponse = await updateDevice(deviceId, name, address);

        console.log(`Save device ${deviceId} with name: ${name}, address: ${address}`);

        if (updateResponse?.id) {
            setEditedDeviceInfo((prevEditedDeviceInfo) => {
                const { [deviceId]: removedDevice, ...rest } = prevEditedDeviceInfo;
                console.log(removedDevice);
                return rest;
            });

            setDevices((prevDevices) => {
                return [
                    ...prevDevices.filter(d => d.id !== deviceId),
                    {
                        ...prevDevices.filter(d => d.id === deviceId)[0],
                        name,
                        address
                    }
                ];
            });
        } else {
            alert('Invalid input!');
        }
    };

    const handleCancelClick = (deviceId) => {
        setEditedDeviceInfo((prevEditedDeviceInfo) => {
            const { [deviceId]: removedDevice, ...rest } = prevEditedDeviceInfo;
            console.log(removedDevice);
            return rest;
        });
    };

    function handleDeviceNameChange(event) {
        const newVal = event.target.value;
    
        setDeviceName(newVal);
    }
    
    function handleDeviceAddressChange(event) {
        const newVal = event.target.value;
    
        setDeviceAddress(newVal);
    }

    function handleDeviceMaximumHourlyEnergyConsumptionChange(event) {
        const newVal = event.target.value;

        setDeviceMaximumHourlyEnergyConsumption(newVal);
    }

    function handleMappingUserIdChange(event) {
        const newVal = event.target.value;
    
        setMappingUserId(newVal);
    }

    function handleMappingDeviceIdChange(event) {
        const newVal = event.target.value;
    
        setMappingDeviceId(newVal);
    }

    async function handleUserDeviceMappingAdd(event) {
        event.preventDefault();

        const response = await addDeviceToUser(mappingDeviceId, mappingUserId);

        console.log(response);

        if (response) {
            confirm('User-device mapping created!');
        } else {
            alert('Invalid input!');
        }

        setMappingUserId('');
        setMappingDeviceId('');
    }
 
    async function handleDeviceAdd(event) {
        event.preventDefault();
        const creationResponse = await createDevice(deviceName, deviceAddress, deviceMaximumHourlyEnergyConsumption);

        console.log(creationResponse);

        if (creationResponse?.id) {
            setDevices([...devices, {
                id: creationResponse.id,
                name: creationResponse.name,
                address: creationResponse.address,
                maximumHourlyEnergyConsumption: creationResponse.maximumHourlyEnergyConsumption
            }]);
        } else {
            alert('Invalid input!');
        }

        setDeviceName('');
        setDeviceAddress('');
        setDeviceMaximumHourlyEnergyConsumption('');
    }

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        async function getDevices() {
            const devicesResponse = await getAllDevices();
            setDevices(devicesResponse);
            console.log(devicesResponse);
        }

        if (isUserLoggedIn && isUserAdmin) { 
            getDevices();
        }
    }, [isUserLoggedIn, isUserAdmin]);

    if (!isAuthInProgress && isUserLoggedIn && isUserAdmin) {
        return (
            <section id="users">
                <div className="container">
                    <h2>Existing devices</h2>
                    <List>
                        {devices?.map((device, index) => (
                            device.name && (
                                <div key={index}>
                                    <ListItem>
                                        <ListItemLink>
                                            <ListItemText className="list-item-text"><strong>Id:</strong> {device.id}</ListItemText>
                                            <ListItemText className="list-item-text"><strong>Max hourly energy consumption:</strong> {device.maximumHourlyEnergyConsumption}</ListItemText>
                                            <ListItemText className="list-item-text">
                                                {editedDeviceInfo[device.id] !== undefined ? (
                                                    <TextField
                                                        value={editedDeviceInfo[device.id].name}
                                                        onChange={(e) => setEditedDeviceInfo({
                                                            ...editedDeviceInfo,
                                                            [device.id]: {
                                                                ...editedDeviceInfo[device.id],
                                                                name: e.target.value,
                                                            },
                                                        })}
                                                        label="Edit Name"
                                                    />
                                                ) : (
                                                    <span><strong>Name</strong>: {device.name}</span>
                                                )}
                                            </ListItemText>
                                            <ListItemText className="list-item-text">
                                                {editedDeviceInfo[device.id] !== undefined ? (
                                                    <TextField
                                                        value={editedDeviceInfo[device.id].address}
                                                        onChange={(e) => setEditedDeviceInfo({
                                                            ...editedDeviceInfo,
                                                            [device.id]: {
                                                                ...editedDeviceInfo[device.id],
                                                                address: e.target.value,
                                                            },
                                                        })}
                                                        label="Edit Address"
                                                    />
                                                ) : (
                                                    <span><strong>Address:</strong> {device.address}</span>
                                                )}
                                            </ListItemText>
                                        </ListItemLink>
                                        {editedDeviceInfo[device.id] !== undefined ? (
                                            <>
                                                <IconButton
                                                    onClick={() => handleSaveClick(device.id)}
                                                    type="button"
                                                    aria-label="save"
                                                    color="primary"
                                                >
                                                    <SaveIcon fontSize="small" />
                                                </IconButton>
                                                <IconButton
                                                    onClick={() => handleCancelClick(device.id)}
                                                    type="button"
                                                    aria-label="cancel"
                                                    color="secondary"
                                                >
                                                    <CloseIcon fontSize="small" />
                                                </IconButton>
                                            </>
                                        ) : (
                                            <>
                                                <IconButton
                                                    onClick={() => handleEditClick(device.id, device.name, device.address)}
                                                    type="button"
                                                    aria-label="edit"
                                                    color="primary"
                                                    style={{ float: 'right' }}
                                                >
                                                    <EditIcon fontSize="small" />
                                                </IconButton>
                                                <IconButton
                                                    onClick={() => {
                                                        console.log(device);

                                                        async function handleScrap() {
                                                            const response = await deleteDevice(device.id);
                                                            const data = response;

                                                            if (data === true) setDevices(devices.filter(d => d.id !== device.id));
                                                            else alert('You do not have the necessary permissions for this action!');
                                                        }

                                                        let result = window.confirm('Are you sure you want to delete this device?');
                                                        if (result === true) {
                                                            handleScrap();
                                                        }
                                                    }}
                                                    type="button"
                                                    aria-label="delete"
                                                    color="primary"
                                                    style={{ float: 'right' }}
                                                >
                                                    <DeleteIcon fontSize="small" />
                                                </IconButton>
                                            </>
                                        )}
                                    </ListItem>
                                    <Divider />
                                </div>
                            )
                        ))}
                    </List>
                </div>
                <br/><br/>
                <section id="add-device">
                    <div className="container">
                        <form type="submit" className="work-form" onSubmit={handleDeviceAdd}>
                            <h2>Create new device</h2>
                            <TextField label="Device name" className="work-form" onChange={handleDeviceNameChange} type="text" autoComplete="off"
                                name="newDeviceName" value={deviceName}/>
                            <br/><br/>
                            <TextField label="Address" className="work-form" onChange={handleDeviceAddressChange} type="text" autoComplete="off"
                                name="newDeviceAddress" value={deviceAddress}/>
                            <br/><br/>
                            <TextField label="Max hourly consumption" className="work-form" onChange={handleDeviceMaximumHourlyEnergyConsumptionChange} type="text" autoComplete="off"
                                name="newDeviceMaximumHourlyEnergyConsumption" value={deviceMaximumHourlyEnergyConsumption}/>
                            <Fab
                                style={{ marginTop: '8px'}}
                                id="work-fab"
                                type="button"
                                aria-label="add"
                                color="primary"
                                size="small"
                                onClick={handleDeviceAdd}
                            >
                                <AddIcon/>
                            </Fab>
                        </form>
                    </div>
                </section>
                <br/><br/>
                <section id="add-user-device-mapping">
                    <div className="container">
                        <form type="submit" className="work-form" onSubmit={handleUserDeviceMappingAdd}>
                            <h2>Create user-device mapping</h2>
                            <TextField label="User id" className="work-form" onChange={handleMappingUserIdChange} type="text" autoComplete="off"
                                name="userId" value={mappingUserId}/>
                            <br/><br/>
                            <TextField label="Device id" className="work-form" onChange={handleMappingDeviceIdChange} type="text" autoComplete="off"
                                name="deviceId" value={mappingDeviceId}/>
                            <Fab
                                style={{ marginTop: '8px'}}
                                id="work-fab"
                                type="button"
                                aria-label="add"
                                color="primary"
                                size="small"
                                onClick={handleUserDeviceMappingAdd}
                            >
                                <AddIcon/>
                            </Fab>
                            <br/><br/>
                        </form>
                    </div>
                </section>
            </section>
        );
    } else if (!isAuthInProgress) {
        window.location.href = '/login';
        return null;
    }
}

export default DeviceDashboard;
