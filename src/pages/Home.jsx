import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import Footer from '../containers/Footer';
import { useAuth } from '../context/AuthContext';

function Home(props) {
    const [resp, ] = useState(false);
    const { refreshAuthorization } = useAuth();

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        if (resp.response === 'failed') {
            props.success('failed');
        } else if (resp.response === 'success') {
            props.success('success');
        }
    }, [props, resp.response]);

    return (
        <div>
            <div id="home">
                <div className="container-fluid">
                    <h1 className="home-title">All your IOT management in one place</h1>
                    <Link to="/login">
                        <button type="button" className="btn">Login to you account</button>
                    </Link>
                </div>
            </div>
            <Footer/>
        </div>
    );
}

export default Home;