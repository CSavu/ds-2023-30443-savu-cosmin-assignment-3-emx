import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Alert from '@mui/material/Alert';
import { useAuth } from '../context/AuthContext';
import { getAllDevicesForUser } from '../services/device-management-service';
import UserChatPopup from '../components/UserChatPopup';

function ListItemLink(props) {
    return <ListItem button component={Link} {...props} />;
}

function Profile() {
    const { isUserLoggedIn, refreshAuthorization, authorizedUserId, isUserAdmin, isAuthInProgress } = useAuth();
    const [devices, setDevices] = useState([]);
    const [userId, setUserId] = useState(null);
    const [deviceAlertingScoket, setDeviceAlertingSocket] = useState(null);
    const [adminChatSocket, setAdminChatSocket] = useState(null);

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        if (deviceAlertingScoket !== null) {
            deviceAlertingScoket.onopen = () => {
                console.log('Device socket connection opened');
            };
    
            deviceAlertingScoket.onmessage = (event) => {
                const message = event.data;
                console.log('Received message on device alerting:', message);
    
                const deviceIdRegex = /deviceId=(\d+)/;
                const deviceIdMatch = message.match(deviceIdRegex);
    
                const lastHourConsumptionRegex = /last hour's consumption=(\d+\.\d{2})\d*/;
                const lastHourConsumptionMatch = message.match(lastHourConsumptionRegex);
                console.log(lastHourConsumptionMatch);
    
                // Check if there is a match and get the device ID
                const deviceId = deviceIdMatch && parseInt(deviceIdMatch[1]);
                const lastHourConsumption = lastHourConsumptionMatch && parseFloat(lastHourConsumptionMatch[1]);
                console.log(deviceId);
                setDevices((prevDevices) => {
                    return prevDevices.map((device) => {
                        if (device.id === deviceId) {
                            console.log({ ...device, status: 'exceeded', lastHourConsumption });
                            return { ...device, status: 'exceeded', lastHourConsumption };
                        }
                        return device;
                    });
                });
            };
    
            deviceAlertingScoket.onclose = () => {
                console.log('Device socket connection closed');
            };
    
            // Clean up the WebSocket connection on component unmount
            return () => {
                deviceAlertingScoket.close();
            };
        }
    }, [deviceAlertingScoket]);

    useEffect(() => {
        setDeviceAlertingSocket(new WebSocket(`wss://${process.env.REACT_APP_API_HOST}:8445/ws?userId=${userId}`));
        setAdminChatSocket(new WebSocket(`wss://${process.env.REACT_APP_API_HOST}:8446/ws/admin-chat?userId=${userId}`));
    }, [userId]);

    useEffect(() => {
        setUserId(authorizedUserId);
    }, [authorizedUserId]);

    useEffect(() => {
        console.log(isUserAdmin);
    }, [isUserAdmin]);

    useEffect(() => {
        async function getAllDevices() {
            const devicesResponse = await getAllDevicesForUser(userId);
            setDevices(devicesResponse);
            console.log(devicesResponse);
        }

        if (userId) getAllDevices();
    }, [userId]);

    useEffect(() => {
        console.log(isUserLoggedIn);
    }, [devices]);

    if (!isAuthInProgress && isUserLoggedIn && !isUserAdmin) {
        return (
            <section id="profile">
                <div className="container">
                    <h2>Assigned devices</h2>
                    <List>
                        {
                            devices?.map((device, index) => {
                                return (device.name && (<div key={index}>
                                    <ListItem>
                                        <ListItemLink onClick={() => window.location.href=`/profile/${device.id}`}>
                                            <ListItemText
                                                className="list-item-text">{device.name}</ListItemText>

                                            <ListItemText
                                                className="list-item-text"><strong>Address:</strong> {device.address}</ListItemText>

                                            <ListItemText
                                                className="list-item-text"><strong>Maximum hourly energy consumption:</strong> {device.maximumHourlyEnergyConsumption}</ListItemText>

                                            { device.status === 'exceeded' && (
                                                <div style={{ marginLeft: '30px', width: '30%', display: 'flex', flexFlow: 'wrap' }}>
                                                    <Alert severity="error">Device maximum hourly energy consumption exceeded with consumption={device.lastHourConsumption}!</Alert>
                                                </div>
                                            )}
                                        </ListItemLink>
                                    </ListItem>
                                    <Divider/>
                                </div>
                                ));
                            })}
                    </List>
                </div>
                <UserChatPopup socket={adminChatSocket} userId={userId} />
            </section>
        );
    } else if (!isAuthInProgress) {
        window.location.href = '/login';
        return null;
    }
}

export default Profile;
