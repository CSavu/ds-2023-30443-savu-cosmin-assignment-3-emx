import React, { useEffect, useState } from 'react';
import { useAuth } from '../context/AuthContext';
import { getById } from '../services/device-management-service';
import { useParams } from 'react-router-dom';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { getAllReadingsForDeviceOnDate } from '../services/monitoring-service';
import dayjs from 'dayjs';
import { Bar } from 'react-chartjs-2';
import Chart from 'chart.js/auto';
import {CategoryScale} from 'chart.js';
import Alert from '@mui/material/Alert';
Chart.register(CategoryScale);

const options = {
    scales: {
        x: {
            type: 'category', // Set the X-axis scale type to 'category'
            display: true, // Display the X-axis
            position: 'bottom', // Position the X-axis at the bottom
            title: {
                display: true,
                text: 'Hour of day', // Customize the X-axis legend
            },
        },
        y: {
            type: 'linear', // Set the Y-axis scale type to 'linear'
            beginAtZero: true, // Start the Y-axis from zero
            display: true, // Display the Y-axis
            position: 'left', // Position the Y-axis on the left
            title: {
                display: true,
                text: 'Measured value', // Customize the Y-axis legend
            },
        }
    }
};

function DevicePage() {
    const { isUserLoggedIn, refreshAuthorization, authorizedUserId, isUserAdmin, isAuthInProgress } = useAuth();
    const [device, setDevice] = useState(null);
    const [userId, setUserId] = useState(null);
    const { deviceId } = useParams();
    const [selectedDate, setSelectedDate] = useState(null);
    const [isDeviceFetchingInProgress, setIsDeviceFetchingInProgress] = useState(true);
    const [readingsData, setReadingsData] = useState([]);
    const [chartData, setChartData] = useState(null);
    const [showNoDataAlert, setShowNoDataAlert] = useState(false);

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        setUserId(authorizedUserId);
    }, [authorizedUserId]);

    useEffect(() => {
        console.log(isUserAdmin);
    }, [isUserAdmin]);

    useEffect(() => {
        async function getDeviceById(deviceId) {
            try {
                const deviceResponse = await getById(deviceId);
                console.log(deviceResponse);
                if (deviceResponse?.status === 500) throw new Error('Device not found');
                setDevice(deviceResponse);
            } catch (error) {
                console.log(error);
                window.location.href = '/profile';
            } finally {
                setIsDeviceFetchingInProgress(false);
            }
        }

        if (userId) getDeviceById(deviceId);
    }, [userId, deviceId]);

    useEffect(() => {
        if (readingsData.length === 0) return;

        const dataPoints = [];
        for (const reading of readingsData) {
            const time = dayjs(reading.timestamp * 1000).format('HH');
            const measuredValue = reading.measuredValue;
            dataPoints.push({ x: time, y: measuredValue });
        }

        const chartToBeRenderedData = {
            labels: dataPoints.length > 0 ? dataPoints.map((point) => point.x) : [],
            datasets: [
                {
                    label: 'Measured Value',
                    data: dataPoints.length > 0 ? dataPoints.map((point) => point.y) : [],
                    backgroundColor: '#5e00da',
                    borderColor: '#5e00da',
                    borderWidth: 1,
                },
            ],
        };

        setChartData(chartToBeRenderedData);
    }, [readingsData]);

    function handleViewConsumption() {
        async function getConsumptionForDeviceOnDate(deviceId, date) {
            const consumptionResponse = await getAllReadingsForDeviceOnDate(deviceId, date);
            console.log(consumptionResponse);
            setReadingsData(consumptionResponse);
            if (consumptionResponse.length === 0) {
                setShowNoDataAlert(true);
                setChartData(null);
            } else setShowNoDataAlert(false);
        }

        const date = dayjs(selectedDate).format('YYYY-MM-DD');
        console.log(date);
        if (date) getConsumptionForDeviceOnDate(deviceId, date);
    }

    if (!isAuthInProgress && isUserLoggedIn && !isUserAdmin && device !== null) {
        return (
            <section id="device">
                <div className="container">
                    <h2>{device?.name}</h2>
                    <h5>Maximum hourly energy consumption: {device?.maximumHourlyEnergyConsumption}</h5>
                    <br/><br/><br/>
                    <h6>View energy consumption by date:</h6>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DemoContainer components={['DatePicker']}>
                            <DatePicker
                                label="Select date"
                                format="YYYY-MM-DD"
                                value={selectedDate}
                                onChange={(newDate) => setSelectedDate(newDate)}
                                clearable
                                autoOk
                            />
                        </DemoContainer>
                    </LocalizationProvider>
                    <br/><br/>
                    <button type="button" className="btn" onClick={handleViewConsumption}>View consumption</button>
                    <br/><br/><br/><br/>
                    { chartData != null && 
                        <div>
                            <Bar data={chartData} options={options} />
                        </div>
                    }
                    {
                        readingsData.length === 0 && showNoDataAlert && (
                            <div style={{ marginLeft: '30px', width: '30%', display: 'flex', flexFlow: 'wrap' }}>
                                <Alert severity="info">No data for selected date!</Alert>
                            </div>
                        )
                    }
                    <br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            </section>
        );
    } else if (!isAuthInProgress && !isDeviceFetchingInProgress) {
        window.location.href = '/profile';
        return null;
    }
}

export default DevicePage;
