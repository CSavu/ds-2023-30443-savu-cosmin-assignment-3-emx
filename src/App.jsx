import React from 'react';
import Header from './containers/Header';
import Profile from './pages/Profile';
import Home from './pages/Home';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from './pages/Login';
import './styles.css';
import UserDashboard from './pages/UserDashboard';
import DeviceDashboard from './pages/DeviceDashboard';
import DevicePage from './pages/DevicePage';

function ClearPathsDefaultRoute() {
    window.location.href = '/';
  
    return (
        <Home />
    );
}

function App() {

    return (
        <Router>
            <Header />
            <Routes>
                <Route exact path="/profile" element={<Profile />} />
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/users" element={<UserDashboard />} />
                <Route exact path="/devices" element={<DeviceDashboard />} />
                <Route exact path="/profile/:deviceId" element={<DevicePage />} />
                <Route exact path="/" element={<Home />} />
                <Route path="*" element={<ClearPathsDefaultRoute />} />
            </Routes>
        </Router>
    );
}

export default App;
